angular.module('starter.controllers', ['ngSanitize'])
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

.controller('SwitchesCtrl', function($scope, $ionicPopup, LokiService) {
  $scope.switches = LokiService.allSwitches();
  //$scope.switches = $scope.switches?$scope.switches:[]; //if null initiate array
  $scope.newSwitch = {name: "", type: "",  letter:"", number: 0, actions: []};
  $scope.hideError=true;
  $scope.insert = function(){
    $scope.switches = LokiService.allSwitches(); //REfresh maybe in other button
    if (!$scope.switches) { //Nothing here
      $ionicPopup.alert({
        title: 'No database',
        template: 'Database not imported!'
      });
      return;
    }
    $ionicPopup.show({
      templateUrl: 'templates/popup-switch.html',
      title: 'New switch',
      scope: $scope,
      buttons: [
        {text: 'Cancel'}, {
          text: '<b>Save</b>',
          type: 'button-positive',
          onTap: function (e) {
            if (!$scope.newSwitch.name || !$scope.newSwitch.type || !$scope.newSwitch.letter || !$scope.newSwitch.number) {
              //don't allow the user to close unless he enters model...
              e.preventDefault();
              $scope.hideError = false;
            } else {
              LokiService.addSwitch($scope.newSwitch);
              $scope.switches = LokiService.allSwitches();
              $scope.newSwitch = {name: "", type: "",  letter:"", number: 0, actions: []};
              $scope.hideError = true;
            }
          }
        }
      ]
    });
  }
  $scope.delete = function(id){
    LokiService.removeSwitch(id);
  }
})

.controller('SensorsCtrl', function($scope, $ionicPopup, LokiService) {
  $scope.sensors = LokiService.allSensors();
  //$scope.sensors = $scope.sensors?$scope.sensors:[]; //if null initiate array
  $scope.newSensor = {name:"", type:"", values:[], switches:[]};
  $scope.hideError=true;
  $scope.insert = function(){
    $scope.sensors = LokiService.allSensors(); //REfresh maybe in other button
    if (!$scope.sensors){ //Nothing here
      $ionicPopup.alert({
        title: 'No database',
        template: 'Database not imported!'
      });
      return;
    }
 /*   $ionicPopup.show({
       templateUrl: 'templates/popup-sensor.html',
        title: 'New sensor',
        scope: $scope,
       buttons: [
          { text: 'Cancel' }, {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.newSensor.name || !$scope.newSensor.type ) {
                //don't allow the user to close unless he enters model...
                e.preventDefault();
                $scope.hideError=false;
              }else {
                LokiService.addSensor($scope.newSensor);
                $scope.sensors = LokiService.allSensors();
                // $scope.sensors.push($scope.newSensor); or  $scope.sensors.push(Object.assign({},$scope.newSensor)); //clone object to sensor array, not needed
                $scope.newSensor = {name:"", type:"", values:[], switches:[]};
                $scope.hideError=true;
              }
            }
          }
        ]
    })
      .then(function(result) {
        if (result !== undefined) {
          console.log('Fuck ')
        } else
          console.log('UnFuck ');

    }); */
  };
})

.controller('SensorDetailCtrl', function($scope, $stateParams, LokiService) {
  $scope.sensor = LokiService.getSensor($stateParams.id);
  $scope.switches = LokiService.allSwitches();

  /*
  $scope.save = ()=>{
    LokiService.updateSensor($scope.sensor);
    $ionicHistory.goBack();

  };*/

})

.controller('AccountCtrl', function($scope, MQTTService) {
  $scope.account = {
    ipaddress:"127.0.0.1",
    port: 1884,
    connected: MQTTService.isConnected() //If you need ti globaly bind iot to service function (not constant)
  };
  $scope.connect = function() {
    //Connect or disconnect
    if ($scope.account.connected) {
      MQTTService.disconnect();
      $scope.account.connected = MQTTService.isConnected();
    } else {
      MQTTService.connect($scope.account.ipaddress, $scope.account.port, function() {
        $scope.$apply(function() {
          $scope.account.connected = MQTTService.isConnected();
        }); //BS, should do it automatically
      });
    }

    $scope.fetchDatabase = function(){
      MQTTService.getConfigDb();
    };

    $scope.uploadDatabase = function(){
      MQTTService.sendConfigDb();
    };
  };
})
.controller('LogCtrl', function($scope, MQTTService, $interval) {
  $scope.log = {txt:''};
  $interval(function(){
    $scope.log.txt = MQTTService.getLog();
  }, 3000); //This automaticly runs $apply
});
