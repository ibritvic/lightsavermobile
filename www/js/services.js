angular.module('starter.services', [])

.factory('LokiService', function(){
  //var adapter = new LokiCordovaFSAdapter({"prefix": "loki"});
  //let db = new loki('configuration', {adapter:adapter});
  var loaded = false; //to prevent load over changed data
  var db = new loki('lights.json' );

  return {
    getDbJSON:function(){
      if (!db) //db must be existant or abort
        return console.log("Get config: no database to get!");

      return db.serialize();  //No need for JSON Stringify
    },
    loadDb:function(jsonTxt){
      if (!db) //db must be existant or abort
        return console.log("Save config: no database to save into!");

      db.loadJSON(jsonTxt);
      db.save();
      loaded = true;
    },
    allSensors:function(){
      var sensors = db.getCollection('sensors');
      if (sensors)
        return sensors.data;
      else
        return false;
    },
    allSwitches:function(){
      var switches = db.getCollection('switches');
      if (switches)
        return switches.data;
      else
        return false;
    },
    getSensor:function(id){
      id = parseInt(id);
      return db.getCollection('sensors').get(id);
    },

    addSwitch:function(sw){
      return db.getCollection('switches').insert(sw);
    },
    removeSwitch:function(id){
      return db.getCollection('switches').remove(id);
    }

  }
})

.factory('MQTTService', ['LokiService',function(LokiService)  { //Array to avoid minifying problem
  var connected=false;
  var client;
  var log="";


   return {
     connect: function(ipaddress, port, callback){
       //PORT only default for now
       client = mqtt.connect('mqtt://' + ipaddress + ':' + port);
       client.on('connect', function() {
         connected = true;
         client.subscribe('server/config');
         client.subscribe('server/log');
         callback();
       });

       client.on('message', function(topic, message)  {
         // message is Buffer
         console.log(message.toString());
         if(topic.indexOf('config') > -1){
           LokiService.loadDb(message);
         }

         if (topic.indexOf('/log') > -1) {
           log =  message + '<br>' + log; //on the top now
           log = log.substr(0, 10000);
         }
       });
     },
     disconnect:  function()  {
       client.end();
       connected = false;
     },
     isConnected: function()  {
       return connected;
     },
     getLog: function()  {
       return log;
     },
     sendConfigDb: function()  {
       if (!connected) //Shouldn't be running at all
        return;

       var dbJSON = LokiService.getDbJSON();
       client.publish('client/config', dbJSON);
     },
     getConfigDb:function(){
       if (!connected) //Shouldn't be running at all
         return;

       client.publish("config/get", "");
     }
   }
}]);
